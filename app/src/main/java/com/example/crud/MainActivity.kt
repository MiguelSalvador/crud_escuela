package com.example.crud

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.crud.databinding.ActivityMainBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var database : DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnIngresar.setOnClickListener {
            val nombres = binding.edtNombre.text.toString()
            val apellidos = binding.edtApellido.text.toString()
            val edad = binding.edtEdad.text.toString()
            val correo = binding.edtCorreo.text.toString()
            val numC = binding.edtNumC.text.toString()

            database = FirebaseDatabase.getInstance().getReference("Usuarios")
            val usuario = User(nombres,apellidos,edad,correo,numC)
            database.child(numC).setValue(usuario).addOnSuccessListener {

                binding.edtNombre.text.clear()
                binding.edtApellido.text.clear()
                binding.edtEdad.text.clear()
                binding.edtCorreo.text.clear()
                binding.edtNumC.text.clear()
                Toast.makeText(this,"Se guardo correctamente", Toast.LENGTH_SHORT).show()

            }.addOnFailureListener {
                Toast.makeText(this,"No se guardo la informacion", Toast.LENGTH_SHORT).show()
            }
        }
        binding.btnLeerDatos.setOnClickListener {
            val intent = Intent(this, Leer::class.java)
            startActivity(intent)
        }

        binding.btnEliminar.setOnClickListener {
            val intent = Intent(this,Borrar::class.java)
            startActivity(intent)
        }

        binding.btnActualizar.setOnClickListener {
            val intent = Intent(this,Buscar::class.java)
            startActivity(intent)
        }
    }
}