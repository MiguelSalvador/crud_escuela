package com.example.crud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.crud.databinding.ActivityActualizarBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Actualizar : AppCompatActivity() {
    private lateinit var binding: ActivityActualizarBinding
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityActualizarBinding.inflate(layoutInflater)
        setContentView(binding.root)
        llenar()

        binding.btnActualizar.setOnClickListener {

            val nombre = binding.EdtNombre.text.toString()
            val apellido = binding.EdtApellidoP.text.toString()
            val edad = binding.EdtEdad.text.toString()
            val correo = binding.EdtCorreo.text.toString()
            val numC = binding.EdtNumC.text.toString()

            updateData(nombre,apellido,edad, correo, numC)
        }
    }

    private fun llenar() {
        val bundle: Bundle? = intent.extras
        bundle?.let {
            binding.EdtNombre.text.append(it.getString("key_name"))
            binding.EdtApellidoP.text.append(it.getString("key_apellidos"))
            binding.EdtEdad.text.append(it.getString("key_edad"))
            binding.EdtCorreo.text.append(it.getString("key_correo"))
            binding.EdtNumC.text.append(it.getString("key_numC"))
        }
    }

    private fun updateData(nombre: String, apellido: String, edad: String, correo: String, numC: String) {

        database = FirebaseDatabase.getInstance().getReference("Usuarios")

        val user = mapOf<String,String>(
            "nombres" to nombre,
            "apellidos" to apellido,
            "edad" to edad,
            "correo" to correo,
            "numC" to numC
        )

        database.child(numC).updateChildren(user).addOnCompleteListener {
            binding.EdtNombre.text.clear()
            binding.EdtApellidoP.text.clear()
            binding.EdtEdad.text.clear()
            binding.EdtCorreo.text.clear()
            binding.EdtNumC.text.clear()

            Toast.makeText(this, "Se actualizo correctamente", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, "No se actualizo ", Toast.LENGTH_SHORT).show()
        }
    }
}